# Name		SFR		sigma		Reference
# ...		Msun/yr		km/s		...
Arp220_West	120.		128.		Scoville+2017
Arp220_East	120.		61.		Scoville+2017
NGC6240		70.		160.		Scoville+2015
Mrk231		176.		60.		Downes&Solomon1998;Veilleux+2009
VIIZw31		66.		21.		Downes&Solomon1998;Sanders+2003
IR10565+2448	163.		29.		Downes&Solomon1998;Veilleux+2009
Arp193		66.		29.		Downes&Solomon1998;Sanders+2003
Mrk273		143.		100.		Downes&Solomon1998;Veilleux+2009
IR17208-0014	428.		107.		Downes&Solomon1998;Veilleux+2009
IR23365+3604	102.		71.		Downes&Solomon1998;Sanders+2003

